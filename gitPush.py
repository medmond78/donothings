import sys

def wait_for_enter():
    input("Press Enter to continue: ")

class addFiles(object):
    def run(self, context):
        print("Add files. (. for all files)")
        #print("   ssh-keygen -t rsa -f ~/{0}".format(context["username"]))
        print("    git add")
        wait_for_enter()

class commitFiles(object):
    def run(self, context):
        print("Commit files using 'git commit -m' and then commit message:")
        #print("    git commit {0}".format(context["username"]))
        print("    git commit -m")
        wait_for_enter()

class pushFiles(object):
    def run(self, context):
        print("Push files to the repository")
        print("    git push")
        wait_for_enter()

if __name__ == "__main__":
    context = {}
    procedure = [
        addFiles(),
        commitFiles(),
        pushFiles(),
    ]
    for step in procedure:
        step.run(context)
        #step.run(self)
    print("Done.")